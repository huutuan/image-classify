import os
import glob

from utils import *

def load_checkpoint(filepath):
    checkpoint = torch.load(filepath)
    model = checkpoint['model']
    model.load_state_dict(checkpoint['state_dict'])
    for parameter in model.parameters():
        parameter.requires_grad = False
    
    model.eval()
    return model


if __name__ == '__main__':
    root_dir = './dataset'
    model_path = 'checkpoint/modelv3.pth'
    # model_path = '../card_classification/checkpoint/modelv2.pth'
    size = (64, 64)

    print('Data loading...')
    transform = transforms.Compose(
        [transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    trainloader, testloader = dataloader(root_dir, transform, size)
    print('Length data : {}, batchsize {}'.format(len(testloader), size))

    print('Model loading...')
    model = load_checkpoint(model_path)

    print('Start testing ...')
    correct = 0
    total = 0
    with torch.no_grad():
        for data in testloader:
            images, labels, _ = data
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

    print('corect {} / total {}'.format(correct, total))
    print('Accuracy of the network on the whole test images: %d %%' % (
        100 * correct / total))
