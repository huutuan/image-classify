import os
import glob

from utils import *


def train(model, trainloader, epoch = 10):
    for epoch in range(epoch):  
        running_loss = 0.0
        for i, data in enumerate(trainloader):
            # get the inputs; data is a list of [inputs, labels, imgpath]
            inputs, labels, _ = data

            # print('inputs, labels : ', inputs.shape, labels.shape)
            # break
            # inputs, labels :  torch.Size([6, 3, 32, 32]) torch.Size([6])

            optimizer.zero_grad()

            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            if (i+1) % 100 == 0:
                print(' Epoch : %d, Mini-batch: %5d] loss: %.3f' %
                    (epoch + 1, i + 1, running_loss))
                running_loss = 0.0
        
    print('Finished Training')



if __name__ == '__main__':
    root_dir = './dataset'
    save_model = 'checkpoint/modelv3.pth'
    input_dim = (3, 64, 64)
    epoch = 5

    # define network
    classifer = Network(input_dim)

    # from inference import loadimage
    # path = '/hdd/tuanlh/card_classification/dataset/cmt/1_6488_blur.jpg'
    # image = loadimage(path)
    # a, b = classifer.calculate_output_dims((3,128, 128))[2:]
    # print('Check network : ', a, b)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(classifer.parameters())

    transform = transforms.Compose(
        [transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    print('>> Load dataset....')
    trainloader, testloader = dataloader(root_dir, transform, (input_dim[1], input_dim[2]))
    print('Length data : ', len(trainloader))

    ## training
    print('>> Start training on {} epochs ...'.format(epoch))
    train(classifer, trainloader, epoch)

    ## save model
    checkpoint = {'model': Network(),
              'state_dict': classifer.state_dict(),
              'optimizer' : optimizer.state_dict()}
    torch.save(checkpoint, save_model)
