import os
import glob

from utils import *


size = (64, 64)

def load_checkpoint(filepath):
    checkpoint = torch.load(filepath)
    model = checkpoint['model']
    model.load_state_dict(checkpoint['state_dict'])
    for parameter in model.parameters():
        parameter.requires_grad = False
    
    model.eval()
    return model


def loadimage(path):
    transform = transforms.Compose(
        [transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    image = cv2.imread(path)
    image = cv2.resize(image, size, interpolation = cv2.INTER_AREA)
    transform(image)

    img = torch.from_numpy(image).reshape(-1, 3, size[0], size[1])
    return img.float()


if __name__ == '__main__':
    path = '/hdd/tuanlh/card_classification/dataset/cmt/1_6488_blur.jpg'
    # path = '/hdd/tuanlh/dataset/backup/blx_field_data/unlabel/32```_30186_warp.jpg'
    # path = '/hdd/tuanlh/dataset/backup/blx_field_data/back/clm2_32083_2.jpg'
    model_path = 'checkpoint/last_model.pth'

    model = load_checkpoint(model_path)

    print('Start testing ...')
    with torch.no_grad():
        image = loadimage(path)
        outputs = model(image)
        print(outputs)
        _, predicted = torch.max(outputs.data, 1)
        if predicted == 1:
            pre_label = 'blx'
        else: 
            pre_label = 'cmt'
    print('Result : ', pre_label)

    img = cv2.imread(path)
    cv2.imshow('Predict result : {}'.format(pre_label), img) 
    cv2.waitKey(0)

# 2345 train/ 585 test