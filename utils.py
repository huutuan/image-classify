import os
import glob

import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset

import matplotlib.pyplot as plt
import numpy as np
import cv2 


__ID__ = {
    'cmt' : 0,
    'blx' : 1
}
classes = ('cmt', 'blx')


## define dataset
class ClasifyData(Dataset):
    def __init__(self, root, mode, size, transform = None, device = 'cuda:0'):
        self.root = './dataset'
        self.mode = mode
        self.size = size
        self.transform = transform
        self.device = device
        self.imgpaths = []
        self.labels = []
        self.process()
    
    def __len__(self):
        return len(self.imgpaths)

    def __getitem__(self, index):
        image = cv2.imread(self.imgpaths[index])
        image = cv2.resize(image, self.size, interpolation = cv2.INTER_AREA)
        if self.transform is not None:
            self.transform(image)
            
        nsize = (3, self.size[0], self.size[1])
        img = torch.from_numpy(image).reshape(nsize)
        
        # img = torch.tensor(img, device=self.device).float()
        return img.float(), self.labels[index], self.imgpaths[index]

    def process(self):
        assert self.mode in ['train', 'test'], 'mode must be train or test !!!!'
        annotation_file = os.path.join(self.root, self.mode) + '.txt'
        with open(annotation_file, 'r') as f:
            data = f.readlines()
        for line in data:
            fpath = line.split('\n')[0]
            self.imgpaths.append(os.path.abspath(os.path.join(self.root, fpath)))
        for line in self.imgpaths:
            k = line.split('/')[-2]
            self.labels.append(__ID__[k])

         
## define network
class Network(nn.Module):
    def __init__(self, input_dims = (3, 64, 64)):
        super(Network, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)

        out_conv_dim = self.calculate_output_dims(input_dims)[2:]
        print('out : ', out_conv_dim)
        self.fc_input = 16 * out_conv_dim[0] * out_conv_dim[1]

        # self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc1 = nn.Linear(self.fc_input, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 2)

    def calculate_output_dims(self, input_dims):
        template = torch.zeros(1, *input_dims)
        template = self.pool(F.relu(self.conv1(template)))
        template = self.pool(F.relu(self.conv2(template)))
        return template.shape

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, self.fc_input)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


def dataloader(root, transform, size = (64, 64)):
    print(size)
    trainset = ClasifyData(root, 'train', size, transform)
    trainloader = DataLoader(trainset, batch_size=16,
                                shuffle=True, num_workers=2)
    testset = ClasifyData(root, 'test', size, transform)
    testloader = DataLoader(testset, batch_size=16,
                                shuffle=False, num_workers=2)
                                
    return trainloader, testloader
