import os

datapath = './dataset'

writetrain = ''
writetest = ''

for cl in os.listdir(datapath):
    clpath = os.path.join(datapath, cl)     # blx, cmt
    files = os.listdir(clpath)
    length = len(files)
    i = 0
    for f in files:
        if i < (0.8 * length):
            writetrain += cl + '/' + f + '\n'
        else:
            writetest += cl + '/' + f + '\n'
        i += 1

with open('./dataset/train.txt', 'w') as f:
    f.write(writetrain)

with open('./dataset/test.txt', 'w') as f:
    f.write(writetest)
